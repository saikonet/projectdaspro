/** Mencoba Membuat Kalkulator 
/** NAMA : HENDRO
/** NIM : H1051171027
*/

// Penerapan inheritance dari javax.swing.JFrame
public class Kalkulator extends javax.swing.JFrame {
	public Kalkulator () {
		initComponents();
	}

@SuppressWarnings("unchecked")

// penerapan Encapsulation
private void initComponents() {

	// Mendeklarasikan Objek Baru
	teksJudul1 = new javax.swing.JTextField();
	teksJudul2 = new javax.swing.JTextField();
	teksJudul3 = new javax.swing.JTextField();
	tomPilihan1 = new javax.swing.JComboBox();
	tomTekan1 = new javax.swing.JButton();
	teksTampil0 = new javax.swing.JLabel();
	teksTampil1 = new javax.swing.JLabel();
	teksTampil2 = new javax.swing.JLabel();
	teksTampil3 = new javax.swing.JLabel();


	setDefaultCloseOperation ( javax.swing.WindowConstants.EXIT_ON_CLOSE );

	teksJudul1.addActionListener ( new java.awt.event.ActionListener () {
		public void actionPerformed ( java.awt.event.ActionEvent evt ) {
			jTextField1ActionPerformed ( evt );
		}
	});

	tomPilihan1.setModel ( new javax.swing.DefaultComboBoxModel ( new String [] { "Tambah", "Kurang", "Kali", "Bagi", "Mod", "Pangkat" }));
	tomPilihan1.addActionListener ( new java.awt.event.ActionListener() {
		public void actionPerformed ( java.awt.event.ActionEvent evt ) {
			jComboBox1ActionPerformed ( evt );
		}
	});


	tomTekan1.setText ( "Lihat Hasil" );
	tomTekan1.addActionListener ( new java.awt.event.ActionListener () {
		public void actionPerformed ( java.awt.event.ActionEvent evt ) {
			jButton1ActionPerformed ( evt );
		}
	});

	teksTampil0.setText ( " KALKULATOR Hehe " );

	teksTampil1.setText ( " Masukkan Angka Pertama " );

	teksTampil2.setText ( " Masukkan Angka Kedua " );

	teksTampil3.setText ( " Hasil Yang Didapat " );


	javax.swing.GroupLayout tampilan = new javax.swing.GroupLayout ( getContentPane ());
	getContentPane () .setLayout ( tampilan );

	// Mengatur Tampilan Horizontal
	tampilan.setHorizontalGroup (
		tampilan.createParallelGroup ( javax.swing.GroupLayout.Alignment.LEADING )
		.addGroup ( tampilan.createSequentialGroup ()
		.addContainerGap ()

	.addGroup ( tampilan.createParallelGroup ( javax.swing.GroupLayout.Alignment.LEADING )
		.addComponent ( teksTampil1 )
		.addComponent ( teksTampil2 )
		.addComponent ( teksTampil3 ))

	.addPreferredGap ( javax.swing.LayoutStyle.ComponentPlacement.RELATED )

	.addGroup ( tampilan.createParallelGroup ( javax.swing.GroupLayout.Alignment.LEADING )
		.addGroup ( tampilan.createSequentialGroup ()
		.addComponent ( teksJudul3, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE )
		.addGap ( 30, 30, 30 )
		.addComponent ( tomTekan1 ))

	.addGroup ( tampilan.createSequentialGroup ()
	.addGroup ( tampilan.createParallelGroup ( javax.swing.GroupLayout.Alignment.LEADING )
		.addComponent ( teksTampil0, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE )
		.addComponent ( teksJudul1, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE )
		.addComponent ( teksJudul2, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE ))
		.addGap ( 39, 39, 39 )

	.addComponent ( tomPilihan1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE )))
		.addContainerGap ( 25, Short.MAX_VALUE ))) ;


	// Mengatur Tampilan Vertikal
	tampilan.setVerticalGroup ( tampilan.createParallelGroup ( javax.swing.GroupLayout.Alignment.LEADING )
		.addGroup ( tampilan.createSequentialGroup ()
		.addContainerGap()

	.addGroup ( tampilan.createParallelGroup ( javax.swing.GroupLayout.Alignment.BASELINE )
		.addComponent ( teksTampil0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE ))
		.addGap ( 20, 20, 20 )

	.addGroup ( tampilan.createParallelGroup ( javax.swing.GroupLayout.Alignment.BASELINE )
		.addComponent ( teksJudul1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE )
		.addComponent ( teksTampil1 ))

	.addPreferredGap ( javax.swing.LayoutStyle.ComponentPlacement.UNRELATED )
	.addGroup ( tampilan.createParallelGroup ( javax.swing.GroupLayout.Alignment.BASELINE )
		.addComponent ( teksTampil2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE )
		.addComponent ( tomPilihan1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE )
		.addComponent ( teksJudul2 ))
		.addGap ( 17, 17, 17 )

	.addGroup ( tampilan.createParallelGroup ( javax.swing.GroupLayout.Alignment.BASELINE )
		.addComponent ( teksTampil3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE )
		.addComponent ( tomTekan1)
		.addComponent ( teksJudul3 ))
	.addContainerGap ( 28, Short.MAX_VALUE )));

	pack();
	}



	private void jButton1ActionPerformed ( java.awt.event.ActionEvent evt ) {

	double angka1 ;
	double angka2 ;
	double angka3 ;
	String Hasil;
	angka1 = Double.valueOf ( teksJudul1.getText ());
	angka2 = Double.valueOf ( teksJudul2.getText ());

	if ( tomPilihan1.getSelectedItem ()=="Tambah" ){
	angka3 = angka1 + angka2 ;
	Hasil = String.valueOf ( angka3 );
	teksJudul3.setText ( Hasil );
	}

	else if ( tomPilihan1.getSelectedItem ()=="Kurang" ) {
	angka3 = angka1-angka2 ;
	Hasil = String.valueOf ( angka3 );
	teksJudul3.setText ( Hasil );
	}

	else if ( tomPilihan1.getSelectedItem () =="Kali" ) {
	angka3 = angka1 * angka2 ;
	Hasil = String.valueOf ( angka3 );
	teksJudul3.setText ( Hasil );
	}

	else if ( tomPilihan1.getSelectedItem () =="Bagi" ) {
	angka3 = angka1 / angka2 ;
	Hasil = String.valueOf ( angka3 );
	teksJudul3.setText ( Hasil );
	}

	else if ( tomPilihan1.getSelectedItem () =="Mod"){
	angka3 = angka1 % angka2 ;
	Hasil = String.valueOf ( angka3 );
	teksJudul3.setText ( Hasil );
	}

	else{
	angka3 = Math.pow( angka1, angka2 );
	Hasil = String.valueOf ( angka3 );
	teksJudul3.setText ( Hasil );
	}

	}

	private void jComboBox1ActionPerformed ( java.awt.event.ActionEvent evt ) {

	}


       private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {                                           
            // Tambahan
       }                                          
   
    
       public static void main(String args[]) {
          
           try {
               for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Bebas".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                      break;
                    }
              }
           } catch (ClassNotFoundException ex) {
               java.util.logging.Logger.getLogger(Kalkulator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                java.util.logging.Logger.getLogger(Kalkulator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
          } catch (IllegalAccessException ex) {
                java.util.logging.Logger.getLogger(Kalkulator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
           } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                java.util.logging.Logger.getLogger(Kalkulator.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
          }
            //</pembuatan fold>
   
          /* membuat dan menampilkan form*/
           java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
               new Kalkulator().setVisible(true);
              }
         });
       }
       // Mendeklarasikan Variabel                   
       public javax.swing.JButton tomTekan1;
       public javax.swing.JComboBox tomPilihan1;
       public javax.swing.JLabel teksTampil0;
       public javax.swing.JLabel teksTampil1;
       public javax.swing.JLabel teksTampil2;
       public javax.swing.JLabel teksTampil3;
       public javax.swing.JLabel teksTampil4;
       public javax.swing.JTextField teksJudul1;
       public javax.swing.JTextField teksJudul2;
       public javax.swing.JTextField teksJudul3;
       // fitur dari java                 
    }
